var MarsMap = require('./mars-map');
var Robot = require('./robot');

/**
 * Dispatch robots
 *
 *
 * Example data:
 *
 *   mapData = {
 *    x: 10,
 *    y: 10,
 *   }
 *
 *   robotData = {
 *     spawn: {
 *       x: 1,
 *       y: 5,
 *       o: 'N',
 *     },
 *     instructions: ['F', 'R', 'F', 'F']
 *   }
 *
 * @param {object} mapData
 * @param {object} robotData
 * @return {array} Robot outputs
 */
function dispatch(mapData, robotData) {
  var outputs = [];

  var map = MarsMap(mapData.x, mapData.y);

  robotData.forEach(function(data) {
    var robot = Robot(data.spawn.x, data.spawn.y, data.spawn.o, map);

    data.instructions.forEach(function(i) {
      robot.move(i);
    });

    outputs.push(robot.getState());
  });

  return outputs;
}

module.exports = dispatch;
