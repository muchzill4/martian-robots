var orientations = ['N','E','S','W'];

/**
 * Create a new robot
 *
 * @param {number} x Integer x-axis spawning point
 * @param {number} y Integer y-axis spawning point
 * @param {string} o Initial orientation (can be: N|S|E|W)
 * @param {MarsMap} map Map module for robot
 * @return {object} Robot instance
 */
function Robot(x, y, o, map) {
  if (!(this instanceof Robot))
    return new Robot(x, y, o, map);

  if (x !== parseInt(x))
    throw new TypeError('X coordinate is invalid');

  if (y !== parseInt(y))
    throw new TypeError('Y coordinate is invalid');

  if (orientations.indexOf(o) < 0)
    throw new TypeError('Invalid orientation');

  this.x = x;
  this.y = y;
  this.o = orientations.indexOf(o);
  this.map = map;

  // Am I spawned on invalid location?
  this.lost = !this.map.isValidLocation(this.x, this.y);
}

Robot.prototype = {
  /**
   * Get current robot state
   *
   * @return {string}
   */
  getState: function() {
    var stateStr = [this.x, this.y, orientations[this.o]].join(' ');

    if (this.lost)
      stateStr += ' LOST';

    return stateStr;
  },

  /**
   * Move robot
   *
   * Robot will ignore unknown movements.
   *
   * Examples:
   *   
   *   robot.move('L') to rotate left
   *   robot.move('R') to rotate right
   *   robot.move('F') to move forward
   *
   * @param {string} instruction What should robot do
   */
  move: function(instruction) {
    if (this.lost)
      return;

    switch (instruction) {
      case 'F': this.moveForward(); break;
      case 'L': this.rotateLeft(); break;
      case 'R': this.rotateRight(); break;
    }
  },

  /**
   * Rotate left
   */
  rotateLeft: function() {
    var len = orientations.length;
    this.o = (this.o + len - 1) % len;
  },

  /**
   * Rotate right
   */
  rotateRight: function() {
    var len = orientations.length;
    this.o = (this.o + 1) % len;
  },

  /**
   * Move forward
   *
   * Robot will get lost if the new location is invalid,
   * unless some other robot, using the same map, got lost in
   * the same location (marked with scent on the map).
   *
   * If robot is lost, it sends the last location before it
   * dissapeared in nothingness of space.
   */
  moveForward: function() {
    var x = this.x;
    var y = this.y;

    switch (orientations[this.o]) {
      case 'N': y += 1; break;
      case 'E': x += 1; break;
      case 'S': y -= 1; break;
      case 'W': x -= 1; break;
    }

    if (!this.map.isValidLocation(x, y)) {
        if (!this.map.hasScent(x, y)) {
          this.map.leaveScent(x, y);
          this.lost = true;
        }

        return;
    }

    this.x = x;
    this.y = y;
  },
};

module.exports = Robot;
