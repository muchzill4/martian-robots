/**
 * Create a new map of Mars
 *
 * @param {number} max_x Integer maximum x-axis coordinate on the map
 * @param {number} max_y Integer maximum y-axis coordinate on the map
 * @return {object} MarsMap instance
 */
function MarsMap(max_x, max_y) {
  if (!(this instanceof MarsMap))
    return new MarsMap(max_x, max_y);

  // Thank god that NaN !== NaN ;)
  if (max_x !== parseInt(max_x))
    throw new TypeError('Max X coordinate is invalid');

  if (max_y !== parseInt(max_y))
    throw new TypeError('Max Y coordinate is invalid');

  this.max_x = max_x;
  this.max_y = max_y;
  this.scents = [];
}

MarsMap.prototype = {
  /**
   * Check location reachability
   *
   * @param {number} x Integer x coord
   * @param {number} y Integer y coord
   * @return {boolean}
   */
  isValidLocation: function(x, y) {
    return (x >= 0 && x <= this.max_x) && (y >= 0 && y <= this.max_y);
  },

  /**
   * Mark location with smell
   *
   * @param {number} x Integer x coord
   * @param {number} y Integer y coord
   */
  leaveScent: function(x, y) {
    this.scents.push([x,y]);
  },

  /**
   * Does location smell?
   *
   * @param {number} x Integer x coord
   * @param {number} y Integer y coord
   * @return {boolean}
   */
  hasScent: function(x, y) {
    return this.scents.some(function(el) {
      return (el[0] === x) && (el[1] === y);
    });
  }
};

module.exports = MarsMap;
