var assert = require('assert');
var dispatch = require('../lib/dispatch');

describe('Dispatch', function() {
  describe('Sending robots to mars', function() {
    it('should send robots and instruct them to move', function() {
      var mapData = { x: 10, y: 10 };
      var robotData = [
        {
          spawn: { x: 0, y: 0, o: 'N' },
          instructions: ['F', 'F', 'F'],
        },
        {
          spawn: { x: 5, y: 1, o: 'N' },
          instructions: ['F', 'R', 'F'],
        },
        {
          spawn: { x: 10, y: 10, o: 'N' },
          instructions: ['R', 'R', 'R', 'R', 'F'],
        }
      ];

      assert.equal('0 3 N', dispatch(mapData, robotData)[0]);
      assert.equal('6 2 E', dispatch(mapData, robotData)[1]);
      assert.equal('10 10 N LOST', dispatch(mapData, robotData)[2]);
    });
  });
});

