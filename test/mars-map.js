var assert = require('assert');
var MarsMap = require('../lib/mars-map');

describe('Mars Map', function() {
  describe('Locations', function() {
    it('should return information about valid locations', function() {
      var m = MarsMap(10, 10);
      assert.equal(true, m.isValidLocation(10, 10));
      assert.equal(true, m.isValidLocation(3, 0));
    });
  });

  describe('Scents', function() {
    it('should return information about invalid locations', function() {
      var m = MarsMap(2, 5);
      assert.equal(false, m.isValidLocation(10, 10));
      assert.equal(false, m.isValidLocation(-1, 5));
    });

    it('should be able to store scent information', function() {
      var m = MarsMap(5, 5);
      m.leaveScent(5, 5);
      m.leaveScent(0, 1);

      assert.equal(true, m.hasScent(5, 5));
      assert.equal(true, m.hasScent(0, 1));

      assert.equal(false, m.hasScent(1, 0));
      assert.equal(false, m.hasScent(20, 0));
    });
  });
});
