var assert = require('assert');
var Robot = require('../lib/robot');
var marsMap = require('../lib/mars-map')(10, 10);

describe('Robot', function() {
  describe('Creating robots', function() {
    it('should create a robot with correct state', function() {
      var r = Robot(1, 1, 'E', marsMap);
      assert.equal('1 1 E', r.getState());
    });

    it('should throw if robot spec is incorrect', function() {
      assert.throws(function() { new Robot([], 1, 'N'); }, TypeError);
      assert.throws(function() { new Robot(2, false, 'N'); }, TypeError);
      assert.throws(function() { new Robot(2, 20, {}); }, TypeError);
    });

    it('should create a lost robot if spawned off bounds', function() {
      var r = Robot(20, 30, 'N', marsMap);
      assert.equal('20 30 N LOST', r.getState());
    });
  });

  describe('Moving robots', function() {
    it('should create a robot that rotates left', function() {
      var r = Robot(1, 1, 'N', marsMap);

      ['W', 'S', 'E', 'N'].forEach(function(orientation) {
        r.move('L');
        assert.equal(r.getState(), '1 1 ' + orientation);
      });
    });

    it('should create a robot that rotates right', function() {
      var r = Robot(1, 1, 'N', marsMap);

      ['E', 'S', 'W', 'N'].forEach(function(orientation) {
        r.move('R');
        assert.equal(r.getState(), '1 1 ' + orientation);
      });
    });

    it('should create a robot that ignores silly instructions', function() {
      var r = Robot(1, 1, 'N', marsMap);
      r.move('trololol');
      r.move(false);
      r.move([]);
      assert.equal(r.getState(), '1 1 N');
    });

    it('should create a robot that moves forward', function() {
      var r = Robot(0, 0, 'N', marsMap);

      r.move('F');
      r.move('F');
      assert.equal(r.getState(), '0 2 N');
    });

    it('should create a robot that performs complex movements', function() {
      var r = Robot(0, 3, 'W', marsMap);
      'LLFFLFLFL'.split('').forEach(function(i) {
        r.move(i);
      });
      assert.equal(r.getState(), '1 4 S');
    });

    it('should create a robot that gets lost', function() {
      var r = Robot(0, 0, 'S', marsMap);
      r.move('R');
      r.move('F');
      assert.equal(r.getState(), '0 0 W LOST');
    });
  });
});
